
/*
D/nC - P1.7 - Pin3   mode select
nSCE - P1.6 - Pin2   chip enable
nRES - P1.5 - Pin1   reset
SDIN - P1.4 - Pin4   serial data line
SCLK - P1.3 - Pin5   serial clock line
            - Pin6   VCC
            - Pin7   BL
            - Pin8   GND
*/

#ifndef __BCD12864_H__
#define __BCD12864_H__


void BCD12864_Reset(void);
void BCD12864_SetXY(unsigned char X, unsigned char Y);
void BCD12864_WriteChar(unsigned char c);
void BCD12864_WriteString(unsigned char* s);
void LCD_PortConfig();

void delay_1ms(unsigned long int param);
void LongClr(unsigned int TclrVal);
void Enter_sleep (void);
void Disp_Logo(unsigned char X, unsigned char Y);
void Clear_disp (void);
void BCD12864_Init (unsigned char disp_mode);
void BCD12864_Reset(void);
void Repeat_set (unsigned char repeat_times1, unsigned char repeat_times2, unsigned char repeat_times3);
void input_command(unsigned char command);
void input_data( unsigned char Data);
void Reflash_disp();

#endif /* __BCD12864_H__ */
